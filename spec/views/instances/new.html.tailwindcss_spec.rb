require 'rails_helper'

RSpec.describe "instances/new", type: :view do
  before(:each) do
    assign(:instance, Instance.new())
  end

  it "renders new instance form" do
    render

    assert_select "form[action=?][method=?]", instances_path, "post" do
    end
  end
end
