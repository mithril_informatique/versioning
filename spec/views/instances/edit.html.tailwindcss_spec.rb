require 'rails_helper'

RSpec.describe "instances/edit", type: :view do
  let(:instance) {
    Instance.create!()
  }

  before(:each) do
    assign(:instance, instance)
  end

  it "renders the edit instance form" do
    render

    assert_select "form[action=?][method=?]", instance_path(instance), "post" do
    end
  end
end
