require 'rails_helper'

RSpec.describe "softwares/new", type: :view do
  before(:each) do
    assign(:software, Software.new())
  end

  it "renders new software form" do
    render

    assert_select "form[action=?][method=?]", softwares_path, "post" do
    end
  end
end
