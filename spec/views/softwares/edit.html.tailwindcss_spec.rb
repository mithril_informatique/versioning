require 'rails_helper'

RSpec.describe "softwares/edit", type: :view do
  let(:software) {
    Software.create!()
  }

  before(:each) do
    assign(:software, software)
  end

  it "renders the edit software form" do
    render

    assert_select "form[action=?][method=?]", software_path(software), "post" do
    end
  end
end
