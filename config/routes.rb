Rails.application.routes.draw do
  devise_for :users
    
  root 'softwares#index'

  resources :softwares, only: [:index, :show] do
    resources :instances, only: [:show]
  end
end
