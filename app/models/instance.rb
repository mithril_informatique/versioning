class Instance < ApplicationRecord
  belongs_to :software

  validates :title, presence: true
  validates :url, presence: true, uniqueness: true
  validates :description, presence: true
end
