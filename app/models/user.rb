class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable,
  # :registerable, :recoverable, :rememberable, :validatable and :omniauthable
  devise :database_authenticatable
end
