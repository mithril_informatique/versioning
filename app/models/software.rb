class Software < ApplicationRecord
  has_many :instances, dependent: :destroy

  validates :title, presence: true, uniqueness: true
  validates :repository, presence: true, uniqueness: true
  validates :description, presence: true
end
