class SoftwaresController < ApplicationController
  # GET /softwares or /softwares.json
  def index
    @softwares = Software.all
  end

  # GET /softwares/1 or /softwares/1.json
  def show
    @software = Software.find(params[:id])
  end
end
