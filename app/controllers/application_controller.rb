class ApplicationController < ActionController::Base
  ApplicationJob.perform_later
  SoftwareJob.perform_later
  InstanceJob.perform_later

  before_action :authenticate_user!
  before_action :data

  def data
    @softwares = Software.all
  end
end
