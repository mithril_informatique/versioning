require 'net/http'
require 'nokogiri'
require 'json'
require "graphql/client"
require "graphql/client/http"

class HTTPRequester
  attr_reader :response

  def initialize(url, method, headers, body)
    @uri = URI(url)

    @http = Net::HTTP.new(@uri.host, @uri.port)
    @http.use_ssl = true if @uri.scheme == 'https'
    @http.verify_mode = OpenSSL::SSL::VERIFY_PEER

    case method.upcase
    when 'GET'
      @request = Net::HTTP::Get.new(@uri.request_uri)
    when 'POST'
      @request = Net::HTTP::Post.new(@uri.request_uri)
      @request.body = body if body
    else
      raise "Unsupported method: #{method}"
    end

    headers.each { |key, value| @request[key] = value }
    @response = @http.request(@request)
  end
end

class HTMLParser
  def initialize(body)
    @html = body
    @hash = html_to_hash
  end

  def html_to_hash
    document = Nokogiri::HTML(@html)
    parse_element(document.root)
  end

  def parse_element(element)
    if element.children.empty?
      element.text
    else
      children = {}

      element.children.each do |child|
        next if child.text? && child.text.strip.empty?

        child_name = child.name.to_sym

        if children[child_name].nil?
          children[child_name] = parse_element(child)
        elsif children[child_name].is_a?(Array)
          children[child_name] << parse_element(child)
        else
          children[child_name] = [children[child_name], parse_element(child)]
        end
      end

      children
    end
  end

  def hash
    @hash
  end
end

class JSONParser
  def initialize(body)
    @json = JSON.parse(body)
  end

  def hash
    @json
  end
end

class DiasporaScraper
  def initialize(url)
    @url = url
    @requester = HTTPRequester.new(@url, 'GET', {}, nil)
    @response = @requester.response
    @parser = HTMLParser.new(@response.body)
  end

  def version
    @parser.hash[:body][:footer][:div][:ul][:li][2][:a][:text].split(' ')[1]
  end
end

class DolibarrScraper
  def initialize(url)
    @url = url
    @requester = HTTPRequester.new(url, 'GET', {}, nil)
    @response = @requester.response
    @parser = HTMLParser.new(@response.body)
  end

  def version
    @parser.hash[:head][:title][:text].split(' ')[2]
  end
end

class EtherPadLiteScraper
  def initialize(url)
    @url = url.dup
    @url << '/api'
    @requester = HTTPRequester.new(@url, 'GET', {}, nil)
    @response = @requester.response
    @parser = JSONParser.new(@response.body)
  end

  def version
    @parser.hash['currentVersion']
  end
end

class MastodonScraper
  def initialize(url)
    @url = url.dup
    @url << '/api/v1/instance'

    @requester = HTTPRequester.new(@url, 'GET', {}, nil)
    @response = @requester.response
    @parser = JSONParser.new(@response.body)
  end

  def version
    @parser.hash['version']
  end
end

class MobilizonScraper
  def initialize(url)
    @url = url.dup
    @url << '/api'

    @http_client = GraphQL::Client::HTTP.new(@url)

    @schema = GraphQL::Client.load_schema(@http_client)

    @client = GraphQL::Client.new(schema: @schema, execute: @http_client)

    @query_string = <<~GQL
      query About {
        config {
          version
        }
      }
    GQL

    begin
      @query = @client.parse(@query_string)
    rescue StandardError => error
      puts "An error occurred: #{error.message}"
    end

    begin
      @hash = @client.query(@query)
    rescue StandardError => error
      puts "An error occurred: #{error.message}"
    end
  end

  def version
    puts @hash

    ''
  end
end

class NextcloudScraper
  def initialize(url)
    @url = url.dup
    @url << '/status.php'

    @requester = HTTPRequester.new(@url, 'GET', {}, nil)
    @response = @requester.response
    @parser = JSONParser.new(@response.body)
  end

  def version
    @parser.hash['versionstring']
  end
end

class PeerTubeScraper
  def initialize(url)
    @url = url.dup
    @url << '/api/v1/config'

    @requester = HTTPRequester.new(@url, 'GET', {}, nil)
    @response = @requester.response
    @parser = JSONParser.new(@response.body)
  end

  def version
    @parser.hash['serverVersion']
  end
end

class WriteFreelyScraper
  def initialize(url)
    @url = url

    @requester = HTTPRequester.new(@url, 'GET', {}, nil)
    @response = @requester.response
    @parser = HTMLParser.new(@response.body)
  end

  def version
    @parser.hash[:body][:footer][:div][:div][:div][1][:ul][:li][3][:text]
  end
end

class ZabbixScraper
  def initialize(url)
    @url = url.dup
    @url << '/api_jsonrpc.php'

    @headers = {
      'Content-Type' => 'application/json-rpc'
    }

    @body = {
      'jsonrpc' => '2.0',
      'method' => 'apiinfo.version',
      'id' => 1,
      'auth' => nil,
      'params' => {}
    }.to_json

    @requester = HTTPRequester.new(@url, 'POST', @headers, @body)
    @response = @requester.response
    @parser = JSONParser.new(@response.body)
  end

  def version
    @parser.hash['result']
  end
end

class InstanceJob < ApplicationJob
  queue_as :default

  after_perform do
    self.class.set(wait: 1.days).perform_later
  end

  def perform
    software_titles_to_ids = Software.pluck(:title, :id).to_h

    scrapers = {
      'Diaspora' => DiasporaScraper,
      'Dolibarr' => DolibarrScraper,
      'EtherPad' => EtherPadLiteScraper,
      'Mastodon' => MastodonScraper,
      # 'Mobilizon' => MobilizonScraper, # TODO: Fix this
      'Nextcloud' => NextcloudScraper,
      'PeerTube' => PeerTubeScraper,
      'WriteFreely' => WriteFreelyScraper,
      'Zabbix' => ZabbixScraper
    }

    Instance.all.find_each do |instance|
      Rails.logger.info "Updating version of #{instance.url}"
      scraper_class = scrapers[software_titles_to_ids.key(instance.software_id)]
      next unless scraper_class

      scraper = scraper_class.new(instance.url)
      begin
        Rails.logger.info "Version: #{scraper.version}"

        instance.update(version: scraper.version) if instance.version != scraper.version
      rescue StandardError => e
        Rails.logger.error("Failed to update version for instance #{instance.id}: #{e.message}")
      end
    end
  end
end
