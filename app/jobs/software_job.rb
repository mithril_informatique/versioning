require 'json'
require 'net/http'
require 'uri'

class GithubVersionScraper
  attr_reader :repository_url

  def initialize(repository_url)
    @repository_url = repository_url
  end

  def fetch_data
    response = request(parsed_url)

    if response.is_a?(Net::HTTPSuccess)
      @json = JSON.parse(response.body)
    else
      Rails.logger.error "Failed to fetch data: #{response.body}"
      @json = []
    end
  end

  def versions
    @json.map { |object| object['name'] }
  end

  def last_version
    words = ['alpha', 'beta', 'r']

    current_version = ''

    versions.each do |version|
      next if words.any? { |word| version.include?(word) }
      current_version = version
    end
    current_version
  end

  private

  def parsed_url
    append_slash_if_needed
    split_parts = @repository_url.split('/')
    extracted_string = "#{split_parts[3]}/#{split_parts[4]}"
    "https://api.github.com/repos/#{extracted_string}/tags"
  end

  def append_slash_if_needed
    @repository_url << '/' unless @repository_url.end_with?('/')
  end

  def request(url)
    uri = URI.parse(url)
    request = Net::HTTP::Get.new(uri)
    request['Accept'] = 'application/vnd.github+json'
    request['X-GitHub-Api-Version'] = '2022-11-28'

    request_options = {
      use_ssl: uri.scheme == 'https'
    }

    Net::HTTP.start(uri.hostname, uri.port, request_options) { |http| http.request(request) }
  end
end

class SoftwareJob < ApplicationJob
  queue_as :default

  after_perform do
    self.class.set(wait: 1.days).perform_later
  end

  def perform
    Software.all.each do |software|
      Rails.logger.info "Updating version of #{software.title}"
      scraper = GithubVersionScraper.new(software.repository)
      scraper.fetch_data
      Rails.logger.info "Last version: #{scraper.last_version}"
      software.update(version: scraper.last_version)
    end
  end
end
