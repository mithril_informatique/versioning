class CreateInstances < ActiveRecord::Migration[7.0]
  def change
    create_table :instances do |t|
      t.string :title
      t.string :url
      t.text :description
      t.string :version

      t.references :software, null: false, foreign_key: true

      t.timestamps
    end
  end
end
