Software.destroy_all

Software.create!([
  {
    title: 'Nextcloud',
    repository: 'https://github.com/nextcloud/server',
    description: 'Powerful & secure cloud server for data management. Collaborate & boost productivity.',
    version: ''
  }, {
    title: 'Dolibarr',
    repository: 'https://github.com/Dolibarr/dolibarr',
    description: 'Modern ERP CRM for businesses. Manage contacts, invoices, stocks & more.',
    version: ''
  }, {
    title: 'Zabbix',
    repository: 'https://github.com/zabbix/zabbix',
    description: 'Real-time IT monitoring of networks, servers, VMs & applications.',
    version: ''
  }, {
    title: 'EtherPad',
    repository: 'https://github.com/ether/etherpad-lite',
    description: 'Truly real-time collaborative document editor for teams.',
    version: ''
  }, {
    title: 'PeerTube',
    repository: 'https://github.com/Chocobozzz/PeerTube',
    description: 'ActivityPub-federated video streaming platform with P2P technology.',
    version: ''
  }, {
    title: 'Mastodon',
    repository: 'https://github.com/mastodon/mastodon',
    description: 'Self-hosted, globally interconnected microblogging community.',
    version: ''
  }, {
    title: 'Mobilizon',
    repository: 'https://github.com/framasoft/mobilizon',
    description: 'Convivial, ethical, & emancipating tool for community organization.',
    version: ''
  }, {
    title: 'Diaspora',
    repository: 'https://github.com/diaspora/diaspora',
    description: 'Privacy-aware, distributed, open-source social network.',
    version: ''
  }, {
    title: 'WriteFreely',
    repository: 'https://github.com/writefreely/writefreely',
    description: 'Clean, Markdown-based publishing platform for collaborative writing.',
    version: ''
  }
])

Instance.destroy_all

# t.string "title"
# t.string "description"
# t.string "url"
# t.string "version"
# t.integer "software_id",

# * nextcloud
# 	* https://cloud.cemea.org
#   * https://cloud.mon.mithril.re
#   * https://cloud1.zourit.net
#   * https://cloud2.zourit.net
#   * https://cloud3.zourit.net
#   * https://cloud4.zourit.net
#   * https://cloud5.zourit.net
#   * https://cloud6.zourit.net
#   * https://cloud6.zourit.net
#   * https://cloud7.zourit.net
#   * https://cloud8.zourit.net

# * dolibarr -
#   * https://dolibarr.mithril.re
#   * https://dolibarr.capelec.re
#   * https://safetysynergy.mithril.re
#   * https://epj.mithril.re

# * zabbix
#   * https://zabbix.mithril.re/zabbix

# * etherped lite
#   * https://pad1.zourit.net
#   * https://pad.mon.mithril.re

# * peertube -
#   * https://runtube.re

# * mastodon -
#   * https://mastodon.re

# * mobilizon -
#   * https://mobilizon.re

# * diaspora -
#   * https://social.re

# * writefreely -
#   * https://blog.mithril.re

# * zimbra
#   * https://zimbra.mithril.re

Instance.create!([
  {
    title: 'CEMEA Cloud',
    description: 'Cloud for CEMEA',
    url: 'https://cloud.cemea.org',
    version: '',
    software_id: Software.find_by(title: 'Nextcloud').id
  }, {
    title: 'Mithril Cloud',
    description: 'Cloud for Mithril',
    url: 'https://cloud.mon.mithril.re',
    version: '',
    software_id: Software.find_by(title: 'Nextcloud').id
  }, {
    title: 'Zourit Cloud 1',
    description: 'Cloud 1 for Zourit',
    url: 'https://cloud1.zourit.net',
    version: '',
    software_id: Software.find_by(title: 'Nextcloud').id
  }, {
    title: 'Zourit Cloud 2',
    description: 'Cloud 2 for Zourit',
    url: 'https://cloud2.zourit.net',
    version: '',
    software_id: Software.find_by(title: 'Nextcloud').id
  }, {
    title: 'Zourit Cloud 3',
    description: 'Cloud 3 for Zourit',
    url: 'https://cloud3.zourit.net',
    version: '',
    software_id: Software.find_by(title: 'Nextcloud').id
  }, {
    title: 'Zourit Cloud 4',
    description: 'Cloud 4 for Zourit',
    url: 'https://cloud4.zourit.net',
    version: '',
    software_id: Software.find_by(title: 'Nextcloud').id
  }, {
    title: 'Zourit Cloud 5',
    description: 'Cloud 5 for Zourit',
    url: 'https://cloud5.zourit.net',
    version: '',
    software_id: Software.find_by(title: 'Nextcloud').id
  }, {
    title: 'Zourit Cloud 6',
    description: 'Cloud 6 for Zourit',
    url: 'https://cloud6.zourit.net',
    version: '',
    software_id: Software.find_by(title: 'Nextcloud').id
  }, {
    title: 'Zourit Cloud 7',
    description: 'Cloud 7 for Zourit',
    url: 'https://cloud7.zourit.net',
    version: '',
    software_id: Software.find_by(title: 'Nextcloud').id
  }, {
    title: 'Zourit Cloud 8',
    description: 'Cloud 8 for Zourit',
    url: 'https://cloud8.zourit.net',
    version: '',
    software_id: Software.find_by(title: 'Nextcloud').id
  }, {
    title: 'Mithril Dolibarr',
    description: 'Dolibarr for Mithril',
    url: 'https://dolibarr.mithril.re',
    version: '',
    software_id: Software.find_by(title: 'Dolibarr').id
  }, {
    title: 'Capelec Dolibarr',
    description: 'Dolibarr for Capelec',
    url: 'https://dolibarr.capelec.re',
    version: '',
    software_id: Software.find_by(title: 'Dolibarr').id
  }, {
    title: 'Safety Synergy Dolibarr',
    description: 'Dolibarr for Safety Synergy',
    url: 'https://safetysynergy.mithril.re',
    version: '',
    software_id: Software.find_by(title: 'Dolibarr').id
  }, {
    title: 'EPJ Dolibarr',
    description: 'Dolibarr for EPJ',
    url: 'https://epj.mithril.re',
    version: '',
    software_id: Software.find_by(title: 'Dolibarr').id
  }, {
    title: 'Mithril Zabbix',
    description: 'Zabbix for Mithril',
    url: 'https://zabbix.mithril.re/zabbix',
    version: '',
    software_id: Software.find_by(title: 'Zabbix').id
  }, {
    title: 'Zourit Pad 1',
    description: 'Pad 1 for Zourit',
    url: 'https://pad1.zourit.net',
    version: '',
    software_id: Software.find_by(title: 'EtherPad').id
  }, {
    title: 'Mithril Pad',
    description: 'Pad for Mithril',
    url: 'https://pad.mon.mithril.re',
    version: '',
    software_id: Software.find_by(title: 'EtherPad').id
  }, {
    title: 'RunTube',
    description: 'PeerTube for Mithril',
    url: 'https://runtube.re',
    version: '',
    software_id: Software.find_by(title: 'PeerTube').id
  }, {
    title: 'Mastodon Réunion',
    description: 'Mastodon for Réunion',
    url: 'https://mastodon.re',
    version: '',
    software_id: Software.find_by(title: 'Mastodon').id
  }, {
    title: 'Mobilizon Réunion',
    description: 'Mobilizon for Réunion',
    url: 'https://mobilizon.re',
    version: '',
    software_id: Software.find_by(title: 'Mobilizon').id
  }, {
    title: 'Social Réunion',
    description: 'Diaspora for Réunion',
    url: 'https://social.re',
    version: '',
    software_id: Software.find_by(title: 'Diaspora').id
  }, {
    title: 'Blog Mithril',
    description: 'WriteFreely for Mithril',
    url: 'https://blog.mithril.re',
    version: '',
    software_id: Software.find_by(title: 'WriteFreely').id
  }
])
