# Ruby
https://ruby-lang.org/

# Rails
https://rubyonrails.org/

# Tailwindcss
https://tailwindcss.com/

# Mechanize
https://github.com/sparklemotion/mechanize

# Catégories de logiciels
* nextcloud
  * cloud1 à 8.zourit.net
	* cloud.cemea.org
 * cloud.mon.mithril.re
* dolibarr -
  * dolibarr.mithril.re
  * dolibarr.capelec.re
  * safetysynergy.mithril.re
  * epj.mithril.re
* zabbix
  * zabbix.mithril.re/zabbix
* etherped lite
  * pad1.zourit.net
  * pad.mon.mithril.re
* jitsi
  * visio1.zourit.net
  * visio2.zourit.net
  * visio.mithril.re
* peertube -
  * runtube.re
* mastodon -
  * mastodon.re
* mobilizon -
  * mobilizon.re
* diaspora -
  * social.re
* writefreely -
  * blog.mithril.re
* zimbra
